package nl.hhs.dsal.week5.opgaven_remove_duplicates;

import nl.hhs.dsal.week5.opgave_bubble_sort.BubbleSorter;
import nl.hhs.dsal.week5.opgave_merge_sort.MergeSorter;

import java.lang.reflect.Array;
import java.util.Arrays;

public class RemoveDuplicates {

    public static int[] removeDuplicates(int[] inp){

        Arrays.sort(inp);

        return null; //AANPASSEN
     }

    public static int[] removeDuplicatesOrdeNlogN(int[] inp){

        //AANVULLEM

        return null; //AANPASSEN
    }




    public static int[] removeElementAtIndex(int index, int[] inp){
        if(index >= inp.length | index < 0) throw new IllegalArgumentException("Unknown index");

        int[] retArr = new int[inp.length -1];

        for(int i = 0; i < inp.length-1; i++){
            if(i < index) retArr[i] = inp[i];
            if(i >= index) retArr[i] = inp[i+1];
        }

        return retArr;
    }
}
