package nl.hhs.dsal.week4.binarytree;

/**
 *
 * @author remcoruijsenaars
 */
public class BinaryTree {
    
    private Node root;
    

    public BinaryTree(Node node){
        root = node;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }


    /**
     * Opgave 1
     * @return de hoogte van de boom.
     */

    public int getHeight(){
        return height(root);
    }


    private int height(Node n){

        return 0;
    }


    /**
     * Opgave 3 (P17.4)
     * @return het aantal knopen dat 1 kind heeft.
     */
    public int countNodesWithOneChild(){
        return 0;
    }

    private int countNodesWithOneChild(Node node){
        return 0;
    }

    /**
     * opgave 5
     * Implementeer inorderTraversal en preOrderTraversal
     * @param v is een object dat gebruikt moet worden om de huidige node te bezoeken.
     */

    public void inorderTraversal(Visitor v){


    }

    private void inOrderTraversal(Node node, Visitor v){

    }


    public void preOrderTraversal(Visitor v){

    }

    private void preOrderTraversal(Node node, Visitor v){

    }


    /**
     * Opgave 6 (EXTRA)
     * @return true waneer de boom gebalanceerd is, false indien dit niet het geval is.
     *
     * De recursieve definitie van een gebalanceerd boom luidt als gevolgd.
     * Het verschil tussen de hoogte van de linker subboom en rechter subboom is maximaal 1.
     *  De linkerboom is gebalanceerd
     *  De rechterboom is gebalanceerd
     *
     */
    public boolean isBalanced(){
        return false;
    }
}
