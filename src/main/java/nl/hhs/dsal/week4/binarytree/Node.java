package nl.hhs.dsal.week4.binarytree;


public class Node {

    private Object data;

    private Node left;
    private Node right;

    public Node(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public boolean oneChild(){
        return (left == null) ^ (right == null);
    }

    public boolean isLeaf(){
        return left==null && right==null;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                '}';
    }
}

