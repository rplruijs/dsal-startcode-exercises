package nl.hhs.dsal.week4.tree;

/**
 *
 * @author remcoruijsenaars
 */
public class Tree {
    
    private Node root;
    
    public Tree(Node root){
        this.root = root;
    }
    
    public int size(){

        if(root == null){
            return 0;
        }else{
            return root.getSize();
        }
    }


    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }
    
    public void addSubTree(Tree subTree){
        root.getChildren().add(subTree.getRoot());
    }
    
    public void addNodeToRoot(Node node){
        root.getChildren().add(node);
    }


    /**
     * Opgave  2 (P17.3)
     * @return het aantal bladeren van een boom.
     */
    public int numberOfLeaves(){
        return 0;
    }

    private int numberOfLeaves(Node n){
        return 0;
    }
    
    
    @Override
    public String toString(){
        return root.toString();
    }

}
