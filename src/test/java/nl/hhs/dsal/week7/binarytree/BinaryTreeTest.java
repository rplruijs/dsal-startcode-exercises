package nl.hhs.dsal.week7.binarytree;

import nl.hhs.dsal.week6.linkedlist.LinkedList;
import nl.hhs.dsal.week6.linkedlist.ListIterator;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BinaryTreeTest {

    BinaryTree searchTreeTestValid;
    BinaryTree searchTreeTestInValid;



    @Before
    public void setUp() throws Exception {
        Node a = new Node(12);
        Node b = new Node(8);
        Node c = new Node(15);
        Node d = new Node(6);
        Node e = new Node(10);
        Node f = new Node(14);
        Node g = new Node(20);

        a.setLeft(b);
        a.setRight(c);

        b.setLeft(d);
        b.setRight(e);

        c.setLeft(f);
        c.setRight(g);

        searchTreeTestValid = new BinaryTree(a);

        Node as = new Node("A");
        Node bs = new Node("D");
        Node cs = new Node("E");
        Node ds = new Node("F");

        bs.setLeft(as);
        as.setLeft(cs);
        as.setRight(ds);

        searchTreeTestInValid = new BinaryTree(bs);




    }



    @Test
    public void isSearchTree_must_return_true_for_valid_searchtree(){
        assert(this.searchTreeTestValid.isSearchTree());
    }

    @Test
    public void isSearchTree_must_return_false_for_invalid_searchtree(){
        assert(!this.searchTreeTestInValid.isSearchTree());
    }


}