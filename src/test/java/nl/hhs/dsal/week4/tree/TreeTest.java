package nl.hhs.dsal.week4.tree;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TreeTest {

    Tree tree;

    @Before
    public void setUp() throws Exception {

        Node question =  new Node("Question");
        Node fillInQuestion = new Node("FillInQuestion");
        Node choiceQuestion = new Node("ChoiceQuestion");
        Node numericQuestion = new Node("NumericQuestion");
        Node freeResponseQuestion = new Node("FreeResponseQuestion");
        Node multiChoiceQuestion = new Node("MultiChoiceQuestion");


        tree = new Tree(question);

        choiceQuestion.addChildNode(multiChoiceQuestion);


        tree.addNodeToRoot(fillInQuestion);
        tree.addNodeToRoot(choiceQuestion);
        tree.addNodeToRoot(numericQuestion);
        tree.addNodeToRoot(freeResponseQuestion);
    }

    @Test
    public void numberOfLeaves() throws Exception {

        final int actualSize = tree.numberOfLeaves();
        final int expectedSize = 4;

        assertEquals(expectedSize, actualSize);

    }

}