package nl.hhs.dsal.week4.binarytree;

import org.junit.Before;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BinaryTreeTest {
    BinaryTree traversalTestTree;

    Node a;
    Node b;
    Node c;
    Node d;
    Node e;
    Node f;
    Node g;


    @Before
    public void setUp() throws Exception {
        a = new Node("A");
        b = new Node("B");
        c = new Node("C");
        d = new Node("D");
        e = new Node("E");
        f = new Node("F");
        g = new Node("G");

        a.setLeft(b);
        a.setRight(c);

        b.setLeft(d);
        b.setRight(e);

        c.setLeft(f);
        c.setRight(g);


        traversalTestTree = new BinaryTree(a);
    }




    @org.junit.Test
    public void getHeight() throws Exception {
        a.setLeft(b);
        b.setLeft(c);
        c.setLeft(d);

        BinaryTree testTree1 = new BinaryTree(a);
        assertEquals(4, testTree1.getHeight());


    }

    @org.junit.Test
    public void countNodesWithOneChild() throws Exception {

        Node a = new Node("A");
        Node b = new Node("B");
        Node c = new Node("C");
        Node d = new Node("D");
        Node e = new Node("E");
        Node f = new Node("F");
        Node g = new Node("G");

        a.setLeft(b);
        a.setRight(c);

        b.setLeft(d);
        b.setRight(e);

        c.setLeft(f);
        c.setRight(g);

        BinaryTree testTree1 = new BinaryTree(a);

        int expectedValue1 = 0;
        int actualValue1 = testTree1.countNodesWithOneChild();

        assertEquals(expectedValue1, actualValue1);


        Node a2 = new Node("A2");
        Node b2 = new Node("B2");
        Node c2 = new Node("C2");
        Node d2 = new Node("D2");
        Node e2 = new Node("E2");

        a2.setLeft(b2);
        b2.setLeft(c2);
        b2.setRight(d2);
        c2.setLeft(e2);

        BinaryTree testTree2 = new BinaryTree(a2);

        int expectedValue2 = 2;
        int actualValue2 = testTree2.countNodesWithOneChild();

        assertEquals(expectedValue2, actualValue2);

    }


    @org.junit.Test
    public void inorderTraversal() throws Exception {
        TestVisitor tv = new TestVisitor();
        traversalTestTree.inorderTraversal(tv);
        List<Node> actual = tv.getNodes();

        List<Node> expected = new ArrayList<>();
        expected.add(d);
        expected.add(b);
        expected.add(e);
        expected.add(a);
        expected.add(f);
        expected.add(c);
        expected.add(g);

        assertEquals(actual, expected);

    }

    @org.junit.Test
    public void preOrderTraversal() throws Exception {
        TestVisitor tv = new TestVisitor();
        traversalTestTree.preOrderTraversal(tv);
        List<Node> actual = tv.getNodes();

        List<Node> expected = new ArrayList<>();
        expected.add(a);
        expected.add(b);
        expected.add(d);
        expected.add(e);
        expected.add(c);
        expected.add(f);
        expected.add(g);

        assertEquals(actual, expected);

    }

    @org.junit.Test
    public void isBalancedTest() throws Exception {

        Node a = new Node("A");
        Node b = new Node("B");
        Node c = new Node("C");
        Node d = new Node("D");
        Node e = new Node("E");
        Node f = new Node("F");
        Node g = new Node("G");

        a.setLeft(b);
        a.setRight(c);
        b.setLeft(d);
        c.setLeft(e);
        e.setLeft(g);

        BinaryTree testTree1 = new BinaryTree(a);
        assert(testTree1.isBalanced() == false);

        Node a2 = new Node("A2");
        Node b2 = new Node("B2");
        Node c2 = new Node("C2");
        Node d2 = new Node("D2");
        Node e2 = new Node("E2");
        Node f2 = new Node("F2");
        Node g2 = new Node("G2");

        a2.setLeft(b2);
        a2.setRight(c2);
        b2.setLeft(d2);
        c2.setLeft(e2);
        c2.setRight(f2);
        e2.setLeft(g2);

        BinaryTree testTree2 = new BinaryTree(a2);
        assert(testTree2.isBalanced());
    }


}