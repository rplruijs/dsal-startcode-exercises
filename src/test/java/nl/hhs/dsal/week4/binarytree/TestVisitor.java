package nl.hhs.dsal.week4.binarytree;

import java.util.ArrayList;
import java.util.List;

public class TestVisitor implements Visitor {
    private List<Node> nodes = new ArrayList<Node>();

    @Override
    public void visit(Node n) {
        nodes.add(n);
    }


    public List<Node> getNodes() {
        return nodes;
    }


}
