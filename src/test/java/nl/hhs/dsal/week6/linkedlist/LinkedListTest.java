package nl.hhs.dsal.week6.linkedlist;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class LinkedListTest {

    LinkedList testInput = new LinkedList();

    @Before
    public void setUp() throws Exception {

        testInput.addFirst(128);
        testInput.addFirst(64);
        testInput.addFirst(32);
        testInput.addFirst(16);
        testInput.addFirst(8);
        testInput.addFirst(4);
        testInput.addFirst(2);

    }

    @Test
    public void getNode_must_return_the_nth_node(){
        int actual = (Integer)testInput.getNode(2).data;
        assertEquals(8, actual);
    }

    @Test
    public void getNode_must_return_the_0_node_correct(){
        int actual = (Integer)testInput.getNode(0).data;
        assertEquals(2, actual);
    }

    @Test
    public void getNode_must_return_null_when_negative_n(){
        LinkedList.Node actual = testInput.getNode(-2);
        assertNull(actual);
    }

    @Test
    public void getNode_must_return_null_when_n_out_of_range(){
        LinkedList.Node actual = testInput.getNode(100);
        assertNull(actual);
    }


    @Test
    public void getNodeRec_must_return_the_nth_node() throws Exception {
        int actual = (Integer)testInput.getNodeRec(2).data;
        assertEquals(8, actual);
    }

    @Test
    public void getNodeRec_must_return_the_0_node_correct(){
        int actual = (Integer)testInput.getNodeRec(0).data;
        assertEquals(2, actual);
    }

    @Test
    public void getNodeRec_must_return_exception_when_negative_n(){
        LinkedList.Node actual = testInput.getNodeRec(-2);
        assertNull(actual);
    }

    @Test
    public void getNodeRec_must_return_exception_when_n_out_of_range(){
        LinkedList.Node actual = testInput.getNode(100);
        assertNull(actual);
    }

    @Test
    public void setNode_must_attach_a_new_data_object_to_the_nth_node(){
        testInput.set(2, new Integer(42));
        Integer actual = (Integer)testInput.getNode(2).data;
        Integer expected = new Integer(42);

        assertEquals(expected, actual);
    }


    @Test(expected = IllegalArgumentException.class)
    public void setNode_must_throw_illegal_argument_exception_when_n_is_out_of_range(){
        testInput.set(-42, new Integer(42));
    }

    @Test
    public void reserse_must_reverse_correctly(){
        testInput.reverse();
        List<Integer> actual = toIntList(testInput);
        List<Integer> expected = Arrays.asList(128, 64, 32, 16, 8, 4, 2);
        assertEquals(expected, actual);

    }

    private List<Integer> toIntList(LinkedList l){

        List<Integer> ret = new ArrayList();
        ListIterator li = l.listIterator();
        while(li.hasNext()){
            ret.add((Integer)li.next());
        }

        return ret;
    }



}