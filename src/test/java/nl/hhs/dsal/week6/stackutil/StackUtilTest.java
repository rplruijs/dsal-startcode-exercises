package nl.hhs.dsal.week6.stackutil;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static org.junit.Assert.assertArrayEquals;

public class StackUtilTest {
    @Test
    public void getOndersteElementen_must_return_the_bottom_n_elements_from_a_stack() throws Exception {
        Stack<Integer> testInput = new Stack<>();
        testInput.push(10);
        testInput.push(9);
        testInput.push(8);
        testInput.push(7);
        testInput.push(6);
        testInput.push(5);
        testInput.push(4);
        testInput.push(3);
        testInput.push(2);
        testInput.push(1);


        List<Integer> actualResult = StackUtil.getOndersteElementen(testInput, 5);
        List<Integer> expectedResult = Arrays.asList(6, 7, 8, 9, 10);

        assertArrayEquals(expectedResult.toArray(), actualResult.toArray());


    }

}