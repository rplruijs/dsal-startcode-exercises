package nl.hhs.dsal.week5.opgaven_remove_duplicates;

import org.junit.Test;

import static org.junit.Assert.*;

public class RemoveDuplicatesTest {


    @Test
    public void removeDuplicateOrdeNKwadraat_should_remove_all_duplicates(){
        int[] testInput = {3, 1, 2, 5, 7, 8, 8, 2, 3};
        int[] actual = RemoveDuplicates.removeDuplicates(testInput);
        int[] expected = {1, 2, 3, 5, 7, 8};

        assertArrayEquals(expected, actual);
    }


    @Test
    public void removeDuplicateOrdeNLogN_should_remove_all_duplicates(){
        int[] testInput = {3, 1, 2, 5, 7, 8, 8, 2, 3};
        int[] actual = RemoveDuplicates.removeDuplicatesOrdeNlogN(testInput);
        int[] expected = {1, 2, 3, 5, 7, 8};

        assertArrayEquals(expected, actual);
    }


    @Test
    public void removeElementAtIndex_should_remove_with_index_in_range(){

        int[] testInput = {3, 1, 2, 5, 7, 8, 8, 2, 3};
        int[] actual = RemoveDuplicates.removeElementAtIndex(3, testInput);
        int[] expected = {3, 1, 2, 7, 8, 8, 2, 3};

        assertArrayEquals(expected, actual);
    }

    @Test
    public void removeElementAtIndex_should_remove_with_index_zero(){

        int[] testInput = {3, 1, 2, 5, 7, 8, 8, 2, 3};
        int[] actual = RemoveDuplicates.removeElementAtIndex(0, testInput);
        int[] expected = {1, 2, 5,  7, 8, 8, 2, 3};

        assertArrayEquals(expected, actual);
    }

    @Test
    public void removeElementAtIndex_should_remove_with_max_index(){

        int[] testInput = {3, 1, 2, 5, 7, 8, 8, 2, 3};
        int[] actual = RemoveDuplicates.removeElementAtIndex(testInput.length-1, testInput);
        int[] expected = {3, 1, 2, 5,  7, 8, 8, 2};

        assertArrayEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeElementAtIndex_should_error_with_negative_index(){
        int[] testInput = {3, 1, 2, 5, 7, 8, 8, 2, 3};
        RemoveDuplicates.removeElementAtIndex(-4, testInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeElementAtIndex_should_error_with_index_out_of_bounds(){
        int[] testInput = {3, 1, 2, 5, 7, 8, 8, 2, 3};
        RemoveDuplicates.removeElementAtIndex(testInput.length, testInput);
    }

}