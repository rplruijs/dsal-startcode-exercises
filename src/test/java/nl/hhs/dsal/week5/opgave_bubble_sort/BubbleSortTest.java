package nl.hhs.dsal.week5.opgave_bubble_sort;

import org.junit.Test;

import static org.junit.Assert.*;

public class BubbleSortTest {
    @Test
    public void bubbleSort_must_sort_a_person_array_correctly_on_age() {


        Persoon remco = new Persoon(39, "Remco");
        Persoon helen = new Persoon(36, "Helen");
        Persoon siem = new Persoon(2, "Siem");
        Persoon isa = new Persoon(0, "Isa");

        Persoon[] inputTest = {remco, helen, siem, isa};
        BubbleSorter.bubbleSort(inputTest);

        System.out.println(inputTest[0]);
        System.out.println(inputTest[1]);
        System.out.println(inputTest[2]);
        System.out.println(inputTest[3]);

        Persoon[] expected = {isa, siem, helen, remco};

        assertArrayEquals(expected, inputTest);

    }

    public void bubbleSort_must_sort_a_Integer_array_correctly_on_age() {

        Integer[] testInput = {3, 4, 51, 78, 4};
    }



    private class Persoon implements Comparable<Persoon>{

        private int leeftijd;
        private String naam;

        public Persoon(int leeftijd, String naam) {
            this.leeftijd = leeftijd;
            this.naam = naam;
        }

        public int compareTo(Persoon o) {
            return this.leeftijd - o.leeftijd;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Persoon persoon = (Persoon) o;

            if (leeftijd != persoon.leeftijd) return false;
            return naam != null ? naam.equals(persoon.naam) : persoon.naam == null;
        }

        @Override
        public int hashCode() {
            int result = leeftijd;
            result = 31 * result + (naam != null ? naam.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Persoon{" +
                    "leeftijd=" + leeftijd +
                    ", naam='" + naam + '\'' +
                    '}';
        }
    }

}